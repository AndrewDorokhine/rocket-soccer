﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RockitFutbal;

namespace RockitFutbal
{
  public enum Team
  {
    RedTeam,
    BlueTeam,
  }

  [System.Serializable]
  public class TeamData {
    [SerializeField] private Team _team;
    private int _currentScore = 0;

    public Team Team { get => _team; set => _team = value; }

    public TeamData(Team team)
    {
        _team = team;
    }
  }
}

[CreateAssetMenu(fileName = "New TeamsData", menuName = "TeamsData", order = 51)]
[System.Serializable]
public class TeamsData : ScriptableObject
{
    
  [SerializeField] private TeamData[] _teams;

  public TeamData[] Teams { get => _teams; set => _teams = value; }
}
