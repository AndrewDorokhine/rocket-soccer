﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RockitFutbal;

public class GoalScript : MonoBehaviour
{
  [SerializeField] private Team _team;
  [SerializeField] private GameObject _gameManager;

  TeamsData _teamsData;

  void Start() {
    SODB sodb = Resources.Load<SODB>("ScriptableObjects/SODB");
    _teamsData = sodb.GetTeamsData();
  }

  private void OnTriggerEnter(Collider collider) {

    //check if this is a soccer ball object
    if(collider.gameObject.tag.Equals("Ball")) {
      foreach(TeamData teamData in _teamsData.Teams) {
        if(teamData.Team != _team) {
          //other teams goal was scored on so we get a point
          _gameManager.SendMessage("IncrementScore", teamData.Team);
        }
      }

      StartCoroutine(Goal(collider.gameObject));
    }
  }

  private IEnumerator Goal(GameObject soccerBall)
	{
    soccerBall.SetActive(false);

    //flashing colors
    gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
		yield return new WaitForSeconds(0.5f);
    gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
		yield return new WaitForSeconds(0.5f);
    gameObject.GetComponent<MeshRenderer>().material.color = Color.yellow;
		yield return new WaitForSeconds(0.5f);
    gameObject.GetComponent<MeshRenderer>().material.color = Color.green;

    //reset game
    soccerBall.transform.position = new Vector3(0f, 10f, 0f);
    soccerBall.transform.rotation = Quaternion.identity;
    Rigidbody rigidbody = soccerBall.GetComponent<Rigidbody>(); 
    rigidbody.velocity = Vector3.zero;
    rigidbody.angularVelocity = Vector3.zero;
    
    _gameManager.SendMessage("ResetLevel");

    soccerBall.SetActive(true);
	}
}
