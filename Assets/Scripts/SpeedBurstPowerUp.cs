﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBurstPowerUp : MonoBehaviour
{
	[SerializeField] private float _value = 25f;

	public float value
	{
		get { return _value; }
		set { _value = value; }
	}

	public IEnumerator Collect()
	{
		gameObject.SetActive(false);
		yield return new WaitForSeconds(5);
		gameObject.SetActive(true); 
	}
}
