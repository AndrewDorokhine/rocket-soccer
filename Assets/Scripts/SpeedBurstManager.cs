﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBurstManager : MonoBehaviour
{
	[SerializeField] private GameObject _powerUpPrefab;

	[SerializeField] private Vector3[] _locations;

    void Start()
	{
		for(int i = 0; i < _locations.Length; i++) {
			Instantiate(_powerUpPrefab, _locations[i], Quaternion.identity);
		}
        
    }

}
