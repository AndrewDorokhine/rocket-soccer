﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RockitFutbal;

namespace RockitFutbal
{

  [System.Serializable]
  public class PlayerData {
    [SerializeField] private Team _team;
    [SerializeField] private bool _isAI;
    [SerializeField] private bool _isP1;
    [SerializeField] private Vector3 _spawnPosition;
    [SerializeField] private Vector3 _spawnRotation;

    public bool IsAI { get => _isAI; set => _isAI = value; }
    public Team Team { get => _team; set => _team = value; }
    public bool IsP1 { get => _isP1; set => _isP1 = value; }
    public Vector3 SpawnPosition { get => _spawnPosition; set => _spawnPosition = value; }
    public Vector3 SpawnRotation { get => _spawnRotation; set => _spawnRotation = value; }

    public PlayerData(Team team, bool isAI, bool isP1)
    {
        _team = team;
        _isAI = isAI;
        _isP1 = isP1;
    }
  }
}

[CreateAssetMenu(fileName = "New PlayersData", menuName = "PlayersData", order = 51)]
[System.Serializable]
public class PlayersData : ScriptableObject
{

  [SerializeField] private PlayerData[] _players;

  public PlayerData[] Players { get => _players; set => _players = value; }

}
