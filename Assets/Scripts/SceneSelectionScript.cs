﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using RockitFutbal;

public class SceneSelectionScript : MonoBehaviour
{
  private SODB _sodb;
	public Button btn_1vAI, btn_1v1, btn_2vAI, btn_2v2;

  void Start()
	{
		btn_1vAI.onClick.AddListener(() => ButtonClicked(0));
		btn_1v1.onClick.AddListener(() => ButtonClicked(1));
		btn_2vAI.onClick.AddListener(() => ButtonClicked(2));
		btn_2v2.onClick.AddListener(() => ButtonClicked(3));

    //need this to store the PlayersData object in later
    _sodb = Resources.Load<SODB>("ScriptableObjects/SODB");
  }

	void ButtonClicked(int buttonNumber) {
    //based on selection, create PlayersData object and load the game scene

    PlayerData player1 = new PlayerData(Team.RedTeam, false, true);
    player1.SpawnPosition = new Vector3(0, 2f, -30f);
    player1.SpawnRotation = Vector3.zero;

    PlayerData player2 = new PlayerData(Team.BlueTeam, false, false);
    player2.SpawnPosition = new Vector3(0f, 2f, 30f);
    player2.SpawnRotation = new Vector3(0, 180, 0);

    PlayersData playersData = (PlayersData) ScriptableObject.CreateInstance( typeof(PlayersData) );

		switch(buttonNumber) {
		case 0:
      //1vAI
      //TODO: use AI!
      playersData.Players = new PlayerData[] {player1};

      _sodb.Objects = new ScriptableObject[] {playersData};

      StartCoroutine(StartGame());
			break;
		case 1:
      //1v1
      playersData.Players = new PlayerData[] {player1, player2};

      _sodb.Objects = new ScriptableObject[] {playersData};

      StartCoroutine(StartGame());
			break;
		case 2:
			Debug.Log("2vAI not supported!");
			break;
		case 3:
			Debug.Log("2v2 not supported!");
			break;
		}
	}

  IEnumerator StartGame()
  {
    Debug.Log("Starting to load game...");

    AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("MainGameScene");

    // Wait until the asynchronous scene fully loads
    while (!asyncLoad.isDone)
    {
        yield return null;
    }
  }
}
