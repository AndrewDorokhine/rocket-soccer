﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SODB", menuName = "ScriptableObjects DB", order = 51)]
public class SODB : ScriptableObject
{
  ScriptableObject[] objects;

  public ScriptableObject[] Objects { get => objects; set => objects = value; }

  public PlayersData GetPlayersData() {
    if(objects != null) {
      foreach(ScriptableObject so in objects) {
        if(so is PlayersData) {
          return (PlayersData) so;
        }
      }
    }

    PlayersData defaultPlayersData = Resources.Load<PlayersData>("ScriptableObjects/DefaultPlayersData");
    return defaultPlayersData;
  }

  public TeamsData GetTeamsData() {
    if(objects != null) {
      foreach(ScriptableObject so in objects) {
        if(so is TeamsData) {
          return (TeamsData) so;
        }
      }
    }

    TeamsData defaultTeamsData = Resources.Load<TeamsData>("ScriptableObjects/DefaultTeamsData");
    return defaultTeamsData;
  }
}
